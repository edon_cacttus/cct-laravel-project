<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class InsertRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            \Illuminate\Support\Facades\DB::beginTransaction();
            \App\Role::create(['name' => 'admin', 'description' => 'Has access everywhere.']);
            \App\Role::create(['name' => 'photographer', 'description' => 'Can upload pictures.']);
            \App\Role::create(['name' => 'editor', 'description' => 'Can create albums.']);
            \Illuminate\Support\Facades\DB::commit();
        }catch (Exception $e){
            \Illuminate\Support\Facades\DB::rollBack();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
