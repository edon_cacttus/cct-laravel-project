
@extends('master');

@section('content')

    <form action="{{route('album.store')}}" enctype="multipart/form-data" method='POST'>
        Name: <input type="text" name="name" />
        Description: <input type="text" name="description"/>
        Pictures: <input  type="file" name="pictures[]" multiple/>
        @csrf
        <button>Create</button>
    </form>

@endsection