@extends("main")

@section('title', "EDIT")

@section("sidebar")

@endsection

@section("content")
    <form action="{{route("profiles.store")}}" method="POST" enctype="multipart/form-data">
        Name: <input type="text" name="name" required/><br>
        Username: <input type="text" name="username"/><br>
        E-Mail Adr:<input type="email" name="email" required/><br>
        Birthdate:<input type="date" name="birthdate"/><br>
        Profile Image: <input type="file" name="profile_picture" /><br>
        @csrf
        <button>Store</button>
    </form>

@endsection
@section("footer")
    @parent
    <h1> Footer 2.0</h1>

    @endsection
