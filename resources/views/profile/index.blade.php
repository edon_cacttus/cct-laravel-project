@extends("main")

@section('title', "Profiles")


@section("content")
    <table>
        <thead>
        <tr>
            <td>Name</td>
            <td>Age</td>
        </tr>
        </thead>
        <tbody>
        @foreach($profiles as $profile)
            <tr>
                <td>{{$profile['name']}}</td>
                <td>{{$profile['age']}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
