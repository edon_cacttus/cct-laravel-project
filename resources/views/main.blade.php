<html>
<head>
    <title>Laravel Project - @yield('title')</title>
</head>
<body>
@section('sidebar')
@show

<div class="container">
    @yield('content')
</div>
<footer>
    @section('footer')
        <p>Footer</p>
    @show
</footer>
</body>
</html>
