<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlbumCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $firstName;
    public $lastName;

    public $attachment;
    private $rawData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($firstName, $lastName, $attachment, $rawData)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->attachment = $attachment;
        $this->rawData = $rawData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject("Welcome to our Platform!")
                    ->view("emails.welcome");

        if(!is_null($this->attachment)){
            $this->attach($this->attachment, [
                'as' => 'Album Cover.png'
            ]);
        }

        if(!is_null($this->rawData)){
            $this->attachData($this->rawData, [
                'as' => 'Album Cover RAW.png',
                'mime' => 'image/png'
            ]);
        }

        $this->withSwiftMessage(function ($message){
            $message->getHeaders()
                ->addTextHeader("Message-ID", time()."__msg_id_".time());
        });

        return $this;
    }
}
