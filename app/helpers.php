<?php

if (!function_exists("has_auth_user_access")){
    function has_auth_user_access(array $roles){
        $user = auth()->user();

        if(empty($user)){
            return false;
        }

        return in_array($user->role_id, $roles);
    }
}
