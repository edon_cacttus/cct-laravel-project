<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "roles";

    protected $fillable = ["name", "description"];

    public $timestamps = false;

    const ROLE_ADMIN = 1;
    const ROLE_PHOTOGRAPHER = 2;
    const ROLE_EDITOR = 3;
}
