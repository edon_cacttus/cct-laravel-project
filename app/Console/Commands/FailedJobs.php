<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FailedJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lproj:failedjobs {connectionName?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command deletes all inactive Albums.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connectionName = $this->argument('connectionName');

        if (!$connectionName) {
            $connectionName = $this->anticipate("Please write the connection name", ['redis', 'database']);
        }

        if(strlen(trim($connectionName)) == 0){
            $this->error("The connection name is not valid!");
            return;
        }

        $password = $this->secret("Please write the password");

        if ($password != "1234") {
            $this->error("Your password is incorrect!");
            return;
        }

        if(!$this->confirm("Do you want to continue?")){
            $this->error("Process terminated!");
            return;
        }


        $failedJobs = DB::select("SELECT 
                                            queue, COUNT(*) as total
                                        FROM
                                            failed_jobs
                                        WHERE
                                            connection = :name
                                        GROUP BY queue", ['name' => $connectionName]);

        if (count($failedJobs) == 0) {
            $this->info( "No Failed Jobs are found for connection: " . $connectionName );
            return;
        }

        /*
         Failed Jobs:
            Connection - {Connection Name}:
                {queue} : {count of failed jobs}
                {queue} : {count of failed jobs}
                {queue} : {count of failed jobs}
                {queue} : {count of failed jobs}
        */
        $this->line("Failed Jobs as Text:");
        $this->line ("\tConnection - " . $connectionName . ":");

        foreach ($failedJobs as $failedJob) {
            $this->line("\t\t" . $failedJob->queue . " : " . $failedJob->total );
        }

        $this->line("Failed Jobs as Table:");
        $this->line ("Connection - " . $connectionName . ":");
        $headers = ['Queue', 'Total'];
        $data    = json_decode(json_encode($failedJobs), true);
        $this->table($headers, $data);
    }
}
