<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DeleteJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lproj:delete-job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command deletes jobs in failed queue.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connectionName = $this->anticipate("Please provide the connection", function ($input){
            $connections = DB::select(DB::raw("select  distinct `connection` from failed_jobs where `connection` like '".$input."%'"));
            return $this->extract($connections, "connection");
        });

        $availableQueueName = DB::select(DB::raw("select  distinct `queue` from failed_jobs where `connection` = '".$connectionName."'"));
        $availableQueueName = $this->extract($availableQueueName, "queue");
        $queueName = $this->choice("Please choose the queue", $availableQueueName, 0);

        $jobs = DB::select(DB::raw("select  * from failed_jobs where `connection` = '".$connectionName."' and queue='".$queueName."'"));

        $bar = $this->output->createProgressBar(count($jobs));

        $bar->start();

        foreach ($jobs as $job){
            sleep(2);
            $bar->advance();
        }

        $bar->finish();
    }

    private function extract(array $data, string $attributeName){
        $toReturn = [];
        foreach ($data as $item){
            $toReturn[] = $item->$attributeName;
        }
        return $toReturn;
    }
}
