<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [ "name", "username", "email" ];

    protected $table = "profiles";
}
