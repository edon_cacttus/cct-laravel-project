<?php

namespace App\Http\Middleware;

use Closure;

class BasicAuthentication
{
    private $users = [
        "username" => "password",
        "admin" => "admin",
        "user1" => "users"
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authorization = $request->headers->get("authorization");
        $authorization = str_replace("Basic ", "", $authorization);
        $decodedValue = base64_decode($authorization, TRUE);
        if(!$decodedValue){
            return response(["message"=>"Unauthorized!"],401);
        }
        $authData = explode(":", $decodedValue);

        if(count($authData) != 2){
            return response(["message"=>"Unauthorized!"],401);
        }

        $username = $authData[0];
        $passowrd = $authData[1];

        if(!isset($this->users[$username])
            || $this->users[$username] !== $passowrd){
            return response(["message"=>"Unauthorized!"],401);
        }

        return $next($request);
    }
}
