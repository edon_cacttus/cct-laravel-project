<?php

namespace App\Http\Middleware;

use Closure;

class ApiKeyAuthentication
{
    private $apiKey = [
        "9bxhdaErnHxWZJQHYcYMRrrUSKnYBBRS45sSH6js",
        "38AtJJ9JtcTeuCA9H2ZVMv97yCfrjhCry6AVtpqA"
    ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiKey = $request->headers->get("api-key");
        if(!in_array($apiKey, $this->apiKey)){
            return response(["message"=>"Unauthorized!"],401);
        }
        return $next($request);
    }
}
