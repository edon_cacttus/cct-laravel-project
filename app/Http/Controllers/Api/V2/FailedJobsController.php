<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FailedJobsController extends Controller
{
    public function index(Request $request, $connectionName)
    {
        return DB::table("failed_jobs")->where('connection', $connectionName)->get();
    }
}
