<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FailedJobsController extends Controller
{
    public function index(Request $request){
        $connectionName = $request->get('cname');
        return DB::table("failed_jobs")->whereIn('connection', $connectionName)->get();
    }

    public function destroy($jobId){
        $failedJob = DB::table("failed_jobs")->where('id', $jobId)->first();

        if(empty($failedJob)){
            return response()->json([
                'message'=>"Job doesn't exist!"
            ], 404);
        }

        DB::table("failed_jobs")->where('id', $jobId)->delete();
        return response()->json([
            'message'=>"Job is deleted!"
        ], 200);
    }

    public function destroyCustom($jobId){
        $failedJob = DB::table("failed_jobs")->where('id', $jobId)->first();

        if(empty($failedJob)){
            return response()->json([
                'success' => false,
                'message'=>"Job doesn't exist!"
            ]);
        }

        DB::table("failed_jobs")->where('id', $jobId)->delete();
        return response()->json([
            'success' => true,
            'message'=>"Job is deleted!"
        ]);
    }

    public function move(Request $request){
        $jobId = $request->get('id');
        $connectionName = $request->get('connectionName');

        $failedJob = DB::table("failed_jobs")->where('id', $jobId)->first();

        if(empty($failedJob)){
            return response()->json([
                'message'=>"Job doesn't exist!"
            ], 404);
        }

        DB::table("failed_jobs")->where('id', $jobId)->update([
            'connection' => $connectionName
        ]);
        return response()->json([
            'message'=>"Job updated!"
        ]);
    }
}
