<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function login(Request $request){
        $this->validate($request, [
            'username' => 'string|required',
            'password' => 'string|required'
        ]);

        $username = $request->get('username');
        $password = $request->get('password');

        $http = new Client();

        $response = $http->post('http://127.0.0.1:8000/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => 'client-secret',
                'username' => 'taylor@laravel.com',
                'password' => 'my-password',
                'scope' => '',
            ],
        ]);

        dd($response);
    }

    public function loginWithLaravelApi(Request $request){
        $this->validate($request, [
            'username' => 'string|required',
            'password' => 'string|required'
        ]);

        $username = $request->get('username');
        $password = $request->get('password');

        $credentials = [
            'email' => $username,
            'password' => $password
        ];

        if(!Auth::attempt($credentials)){
            return response()->json([
                'message' => 'Credentials do not match!'
            ], 401);
        }

        $token = hash('sha256', Str::random(60));

        Auth::user()->forceFill([
            'api_token' => $token,
        ])->save();

        return response()->json([
            'message' => 'Credentials do match',
            'api_token' => $token
        ],200);
    }
}
