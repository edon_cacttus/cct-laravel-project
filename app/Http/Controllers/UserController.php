<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function createRandomProfile(){
        $profile = new Profile();

        $profile->name = "Filan" . time();
        $profile->username = time();
        $profile->email = time();
        $profile->birthdate = "1994-02-23";
        $profile->is_active = true;

        $profile->save();

        return "OK";
    }

    public function storeProfileImage(Request $request){
        $path = $request->file("profile_picture")->store("public/profile_images/");

        return view('upload');
    }

    public function getAll() {
        $id = "1 OR 1 =1 ";
        $profiles = Profile::whereRaw('`username` = `email`')
            ->get();

        return response()->json($profiles);
    }

    public function updateProfile($id){
        $profile = Profile::find($id);

        if ($profile){
            $profile->name .= "2";

            $profile->save();
        }else{
            echo "404";
        }


    }

    public function myProfile($id = null, $image=null){
        $profiles = [];
        for($i = 0; $i < 20; $i++) {
            $age =  rand(1, 100);

            if($i%2==0){
                $name  = "Filan_".$i;
            }
            else{
                $name  = "Filane_".$i;
            }

            $profiles[] = [
                'age' => $age,
                'name'  => $name
            ];
        }

        return view('profile', [
            'profiles' => $profiles
        ]);
    }
}
