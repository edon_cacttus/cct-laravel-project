<?php

namespace App\Http\Controllers;

use App\Album;
use App\Jobs\AlbumStatus;
use App\Picture;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AlbumController extends Controller
{
    //
    public function  getCreate() {
        return view('album.create');
    }

    public function getView($albumId){
        $album = Album::findOrFail($albumId);

        if(!$album->status){
            echo "The Ablum [".$album->name."] is not processed.";
        }else{
            echo "Album [".$album->name."] Details";
        }
    }

    public function store(Request $request){

        $album = new Album();
        $album->name = $request->get('name');
        $album->description = $request->get('description');
        $album->status = false;
        $album->save();

        $albumId = $album->id;

        $pictures = $request->allFiles();
        $pictures = $pictures['pictures'];

        $count = count($pictures);
        
        if($count>5){
            
            return "You exceeded the limit of pictures";
        }
        foreach ($pictures as $picture) {
            $path = $picture->store('public/pictures');
            $pictureModel = new Picture();

            $pictureModel->path = $path;
            $pictureModel->album_id = $albumId;
            $pictureModel->save();
        }
        AlbumStatus::dispatch($album)->delay(now()->addSeconds(30));
        return "Album and Pictures are saved successfully";
    }
}
