<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FailedJobsController extends Controller
{
    public function index(){
        return DB::select("SELECT * FROM failed_jobs");
    }
}
