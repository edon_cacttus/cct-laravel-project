<?php

namespace App\Http\Controllers;

use App\Jobs\ActivateProfiles;
use App\Jobs\DeactivateProfiles;
use App\Mail\AlbumCreated;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    /*
     * Show profiles
     */
    public function index(){
        return view('profile.index');
    }

    public function getCreate(){
        return view('profile.create');
    }

    public function deactiveAllProfiles(){
        DeactivateProfiles::dispatch();
        DeactivateProfiles::dispatch();
        ActivateProfiles::dispatch();
        ActivateProfiles::dispatch();
        DeactivateProfiles::dispatch();
        DeactivateProfiles::dispatch();
    }

    public function sendDummyEmail(){
        //Mail
        $filename = "/Users/edonsekiraqa/cacttus/laravel_project/storage/app/public/profiles/ORyzGMxuTMlFsLDHmJpOwzxFpsXnVz9Awx5pnNyD.png";
        Mail::to(["test2@cacttus.com"])->send(new AlbumCreated("Filan", "Fisteku", $filename, null));

        //Mail, cc, bcc
        Mail::to(["test2@cacttus.com"])
            ->cc(["test3@cacttus.com"])
            ->bcc(["test4@cacttus.com"])
            ->send(new AlbumCreated("Filan", "Fisteku", $filename, null));

        //Mail with queue
        Mail::to(["test2@cacttus.com"])
            ->queue(new AlbumCreated("Filan", "Fisteku", $filename, null));

        //Mail with schedule
        Mail::to(["test2@cacttus.com"])
            ->later(now()->addSeconds(10),new AlbumCreated("Filan", "Fisteku", $filename, null));

        //Mail with specific queue
        $message = new AlbumCreated("Filan", "Fisteku", $filename, null);
       // $message->onQueue("mail");
        Mail::to(["test2@cacttus.com"])
          ->queue($message);
        Mail::to(["test2@cacttus.com"])->send(new AlbumCreated("Filan", "Fisteku", $filename, null));

        //Mail, cc, bcc
        Mail::to(["test2@cacttus.com"])
            ->cc(["test3@cacttus.com"])
            ->bcc(["test4@cacttus.com"])
            ->send(new AlbumCreated("Filan", "Fisteku", $filename, null));

        //Mail with queue
        Mail::to(["test2@cacttus.com"])
            ->queue(new AlbumCreated("Filan", "Fisteku", $filename, null));

        //Mail with schedule
        Mail::to(["test2@cacttus.com"])
            ->later(now()->addSeconds(10),new AlbumCreated("Filan", "Fisteku", $filename, null));

        //Mail with specific queue
        $message = new AlbumCreated("Filan", "Fisteku", $filename, null);
        // $message->onQueue("mail");
        Mail::to(["test2@cacttus.com"])
            ->queue($message);

    }

    public function trackMail($emailId){
        Log::info("Email with ID={".$emailId."} is opened!");
    }

    /*
     * Store data for new profile
     */
    public function store(Request $request){
        $data = $request->all();

        $imagePath = $data['profile_picture']->store("public/profiles");

        $profile = new Profile();
        $profile->name = $data["name"];
        $profile->username = $data["username"];
        $profile->email = $data["email"];
        $profile->birthdate = $data["birthdate"];
        $profile->is_active = true;
        $profile->image = $imagePath;

        try {
            $profile->save();
        }catch (\Exception $e){
            Storage::delete($imagePath);
            echo $e->getMessage();
        }
    }
}
