<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', 'Api\Auth\LoginController@login');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Authentication methods: api_auth, basic_auth
Route::group(['middleware'=>'basic_auth'], function (){
    Route::group(['prefix'=>'v1'], function(){
        Route::get("/failed-jobs", "Api\FailedJobsController@index");
        Route::delete("/failed-jobs/{jobId}", "Api\FailedJobsController@destroy");
        Route::delete("/failed-jobs-c/{jobId}", "Api\FailedJobsController@destroyCustom");
        Route::put("/failed-jobs/move", "Api\FailedJobsController@move");
    });

    Route::group(['prefix'=>'v2'],function(){
        Route::get("/failed-jobs/{connectionName}", "Api\V2\FailedJobsController@index");
    });
});


