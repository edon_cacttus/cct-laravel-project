<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Mail;

Route::get('/', function () {
    Mail::raw('Text', function ($message){
        $message->to('contact@contact.com');
    });
    return view('master');
});

Route::get('/deactiveAllProfiles', 'ProfileController@deactiveAllProfiles');
Route::get('/sendDummyEmail', 'ProfileController@sendDummyEmail');
Route::get('/imgOpen/{emailId}', 'ProfileController@trackMail')->name('track.email');



Route::group(['middleware'=>['auth', 'verified']],function(){
    Route::get('/profiles', 'ProfileController@index')->name("profiles.index");
    Route::get('/create-profile', 'ProfileController@getCreate')->name("profiles.create");
    Route::post('/store-profile', 'ProfileController@store')->name("profiles.store");

    Route::get('/my-profile/{id?}/images/{image?}', 'UserController@myProfile');
    Route::post('/createRandomProfile', 'UserController@createRandomProfile');
    Route::view('/edit', 'edit');

    Route::group(['prefix'=>'album', 'middleware'=>['role:'.\App\Role::ROLE_ADMIN.",".\App\Role::ROLE_EDITOR]], function (){
        Route::get('/view/{albumId}', 'AlbumController@getView')->name('album.getView');
        Route::get('/create', 'AlbumController@getCreate')->name('album.getCreate');
        Route::post('/store','AlbumController@store')->name('album.store');
    });

    Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
});

Route::get("/failed-jobs", "FailedJobsController@index");
Auth::routes(['verify' => true]);

